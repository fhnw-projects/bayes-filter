package ch.fhnw.dist.bayes;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class BayesFilter {

    public static void main(String[] args) {

        // Parses all files of a zip and saves the words in a map with its occurrences.
        ZipParser ham = new ZipParser("ham-anlern.zip");
        ZipParser spam = new ZipParser("spam-anlern.zip");

        // Gets the maps with the percentages
        Map<String, Double> percentageHam = BayesClassificator.getPercentageMap(ham.getMap(), ham.getEmailCounter());
        Map<String, Double> percentageSpam = BayesClassificator.getPercentageMap(spam.getMap(), spam.getEmailCounter());

        System.out.println("Threshold for classifying mail as spam: " + BayesClassificator.THRESHOLD_CLASSIFICATION);
        System.out.println("Threshold for Ω : " + BayesClassificator.THRESHOLD);

        // Parses the ham-test files into a list of sets
        ZipParser parser = new ZipParser();
        List<Set<String>> testFilesHam = parser.parseFile("ham-test.zip");

        int counterHamMail = 0;
        int hamClassified = 0;
        System.out.println("---- Start Ham-Mail classification ----");
        for (Set<String> set : testFilesHam) {
            counterHamMail++;
            boolean isSpam = BayesClassificator.isSpam(set, percentageHam, percentageSpam);
            if (!isSpam) {
                hamClassified++;
            }
            // System.out.println("Mail" + counterHamMail + " classified as: " + (isSpam ? "Spam" :
            // "Ham"));
        }
        System.out.println("Ratio correct classified: " + (double) hamClassified / counterHamMail);

        // Parses the spam-test files into a list of sets
        List<Set<String>> testFilesSpam = parser.parseFile("spam-test.zip");

        int counterSpamMail = 0;
        int spamClassified = 0;
        System.out.println("---- Start Spam-Mail classification ----");
        for (Set<String> set : testFilesSpam) {
            counterSpamMail++;
            boolean isSpam = BayesClassificator.isSpam(set, percentageHam, percentageSpam);
            if (isSpam) {
                spamClassified++;
            }
            // System.out.println("Mail" + counterSpamMail + " classified as: " + (isSpam ? "Spam" :
            // "Ham"));
        }
        System.out.println("Ratio correct classified: " + (double) spamClassified / counterSpamMail);
    }

}
