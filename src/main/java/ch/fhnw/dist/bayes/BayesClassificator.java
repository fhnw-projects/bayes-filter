package ch.fhnw.dist.bayes;

import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class BayesClassificator {

    // Best effects between 0.001 -0.009
    public static final double THRESHOLD = 0.002;
    public static final double THRESHOLD_CLASSIFICATION = 0.9;

    /**
     * limits the map with any significants words
     * 
     * @param map
     * @param limit
     * @return
     */
    public static Map<String, Integer> getSignificantMap(Map<String, Integer> map, int limit) {
        return map.entrySet().stream().sorted(Collections.reverseOrder(Map.Entry.comparingByValue())).limit(limit)
                .collect(Collectors.toMap(e -> e.getKey(), e -> e.getValue()));
    }

    /**
     * saves the percentages in a map, static value if > 0.99 or < 0.01
     * 
     * @param map
     * @param emailCounter
     * @return
     */
    public static Map<String, Double> getPercentageMap(Map<String, Integer> map, int emailCounter) {
        return map.entrySet().stream().collect(Collectors.toMap(e -> e.getKey(), e -> {
            double val = (double) e.getValue() / emailCounter;
            if (val < 0.01) {
                return 0.01;
            } else if (val > 0.99) {
                return 0.99;
            } else {
                return val;
            }
        }));
    }

    /**
     * Calculates the probability and checks if the mail i spam or not by applying Bayes rule
     * 
     * @param set
     * @param percentageHam
     * @param percentageSpam
     * @return
     */
    public static boolean isSpam(Set<String> set, Map<String, Double> percentageHam,
            Map<String, Double> percentageSpam) {

        // Percentage can't be 0 if the word is present in only one list (something in a
        // multiplication * 0 always results in 0, thats why the threshold is applied).

        double s1 = 1.0;
        double s2 = 1.0;
        // Remove empty String
        set.remove("");
        for (String word : set) {
            if (percentageHam.containsKey(word) || percentageSpam.containsKey(word)) {
                double spamProb = percentageSpam.getOrDefault(word, THRESHOLD);
                double hamProb = percentageHam.getOrDefault(word, THRESHOLD);
                s1 *= spamProb;
                s2 *= hamProb;
            }
        }
        double perc = s1 / (s1 + s2);
        // System.out.println("Spam-Percentage: " + perc);
        if (perc >= THRESHOLD_CLASSIFICATION) {
            return true;
        }
        return false;
    }

}
