package ch.fhnw.dist.bayes;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class ZipParser {

    private Map<String, Integer> map;
    private int emailCounter = 0;

    public ZipParser(String... zipPath) {
        parseZip(zipPath);
    }

    public ZipParser() {
    }

    /**
     * Puts all words of the files in a map with its occurrences.
     * 
     * @param zipPath the path of the zip file
     * @return the amount of file parsed
     */
    private void parseZip(String... zipPath) {

        for (String path : zipPath) {
            Map<String, Integer> map = new HashMap<>();
            try (ZipFile zipFile = new ZipFile(getClass().getClassLoader().getResource(path).getFile())) {

                // Gets all entries of a zip file
                Enumeration<? extends ZipEntry> entries = zipFile.entries();

                // read every file in the zip and save all words in the map with their occurrences
                while (entries.hasMoreElements()) {
                    this.emailCounter++;
                    ZipEntry entry = entries.nextElement();
                    try (BufferedReader br = new BufferedReader(
                            new InputStreamReader(zipFile.getInputStream(entry), "UTF-8"))) {
                        String line = br.readLine();
                        while (line != null) {
                            String[] words = line.split("\\s+");
                            for (String word : words) {
                                Integer i = map.get(word);
                                // sets the word in the map or increases its occurrence if it
                                // appears in
                                // a different mail
                                if (i != null) {
                                    if (i < this.emailCounter) {
                                        map.put(word, i + 1);
                                    }
                                } else {
                                    map.put(word, 1);
                                }
                            }
                            line = br.readLine();
                        }
                    }
                }
                this.map = map;

            } catch (IOException e) {
            }
        }
    }

    /**
     * Parses the files in a List of Sets.
     * 
     * @param zipPath
     * @return
     */
    public List<Set<String>> parseFile(String zipPath) {

        try (ZipFile zipFile = new ZipFile(getClass().getClassLoader().getResource(zipPath).getFile())) {
            // Gets all entries of a zip file
            Enumeration<? extends ZipEntry> entries = zipFile.entries();

            List<Set<String>> parsedFiles = new ArrayList<>();
            // read every file in the zip and save all words in a map with their occurrences
            while (entries.hasMoreElements()) {
                Set<String> set = new HashSet<>();
                ZipEntry entry = entries.nextElement();
                try (BufferedReader br = new BufferedReader(
                        new InputStreamReader(zipFile.getInputStream(entry), "UTF-8"))) {
                    String line = br.readLine();
                    while (line != null) {
                        String[] words = line.split("\\s+");
                        for (String word : words) {
                            // sets the word in the map or increases its occurrence if it appears
                            // multiple times
                            set.add(word);
                        }
                        line = br.readLine();
                    }
                }
                parsedFiles.add(set);
            }
            return parsedFiles;
        } catch (IOException e) {
            return null;
        }
    }

    public Map<String, Integer> getMap() {
        return map;
    }

    public void setMap(Map<String, Integer> map) {
        this.map = map;
    }

    public int getEmailCounter() {
        return emailCounter;
    }

    public void setEmailCounter(int emailCounter) {
        this.emailCounter = emailCounter;
    }

}
